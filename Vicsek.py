from graphics import *
from math import *
from random import *
import time


def main():

    space_width = 700
    space_height = 700
    margin = 100
    max_vel = 3
    interaction_radius = 60
    disturbance = 0
    agents_numbers = 100
    agent = []
    tail = 5
    agent_history = []
    h_x = []
    h_y = []

    def distance(p0, p1):
        return sqrt((p0[0] - p1[0]) ** 2 + (p0[1] - p1[1]) ** 2)

    def rotate(l, n):
        return l[-n:] + l[:-n]

    # assigning agents with random initial setup values
    for i in range(agents_numbers):
            # agent(positionX,positionY,velocity,Orientation)
            agent.append([
                randint(0 + margin, space_width - margin),
                randint(0 + margin, space_height - margin),
                max_vel,
                random() * 2*pi,
                0.0])

            agent_history.append([
                h_x,
                h_y
            ])
    for i in range(agents_numbers):
        for j in range(tail):
            agent_history[i][0].append(agent[i][0])
            agent_history[i][1].append(agent[i][1])

    win = GraphWin("Simple Swarm", space_width, space_height)
    win.setBackground(color_rgb(90, 5, 30))

    while True:
            time_inital = time.time()

            for j in range(agents_numbers):

                x = agent[j][0]
                y = agent[j][1]
                v = agent[j][2]
                o = agent[j][3]
                # updating new positions
                x += v * cos(o)
                y += v * sin(o)
                # Periodic boundary implementation
                if x < 0:
                    x = x + space_width
                    # y = y - space_height

                if y < 0:
                    y = y + space_height
                    # x = x - space_width

                if x > space_width:
                    x = x - space_width

                if y > space_height:
                    y = y - space_height

                agent[j][0] = x
                agent[j][1] = y

                pt = Point(x, y)
                pt.setOutline(color_rgb(200, 230, 190))
                pt.draw(win)

                agent_history[j][0] = rotate(agent_history[j][0], 1)
                agent_history[j][1] = rotate(agent_history[j][1], 1)

                agent_history[j][0][0] = agent[j][0]
                agent_history[j][1][0] = agent[j][1]

                pt = Point(agent_history[j][0][tail], agent_history[j][1][tail])
                pt.setOutline(color_rgb(90, 5, 30))
                pt.draw(win)

            # here is the main! code:
            for t in range(agents_numbers):
                # searching for other agents in the interaction area of agent[t]
                mean_x = 0
                mean_y = 0
                interacting_agents = 0
                for q in range(agents_numbers):
                        if distance(agent[t], agent[q]) <= interaction_radius:

                            interacting_agents += 1
                            mean_x += cos(agent[q][3])
                            mean_y += sin(agent[q][3])

                agent[t][4] = atan2((mean_y / interacting_agents), (mean_x / interacting_agents))
            # agent[t][4] is the new value for agent[t][3]
            # but can't update it in the above loop because it will affect other agent's angle calculation
            for i in range(agents_numbers):
                agent[i][3] = agent[i][4] + (random()-0.5) * disturbance

            print(time.time() - time_inital)


main()
