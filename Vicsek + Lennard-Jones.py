from graphics import *
from math import *
from random import *


def main():

    space_width = 500
    space_height = 500
    margin = 50
    max_vel = 10
    interaction_radius = 70
    disturbance = 0.2
    agents_numbers = 50
    agent = []
    w_mean = 1
    w_lenn = 0
    tail = 10
    agent_history = []
    h_x = []
    h_y = []

    # lennardJones parameters
    e_factor = 0.4
    s_factor = 60

    def distance(p0, p1):
        return sqrt((p0[0] - p1[0]) ** 2 + (p0[1] - p1[1]) ** 2)

    def lennard(p0, p1):
        d = distance(p0, p1)
        if d == 0:
            return 0
        else:
            return e_factor * (((s_factor/d)**3) - (1 * ((s_factor/d)**2))) - 0.00

    def rotate(l, n):
        return l[-n:] + l[:-n]

    # assigning agents with random initial setup values
    for i in range(agents_numbers):
            # agent(positionX,positionY,velocity,Orientation)
            agent.append([
              randint(0 + margin, space_width - margin),
              randint(0 + margin, space_height - margin),
              max_vel * 2*(random() - 0.5),
              max_vel * 2*(random() - 0.5),
              0.0,
              0.0,
              0.0,
              0.0

              ])

            agent_history.append([
                h_x,
                h_y
            ])

    for i in range(agents_numbers):
        for j in range(tail):
            agent_history[i][0].append(agent[i][0])
            agent_history[i][1].append(agent[i][1])

    win = GraphWin("Simple Swarm", space_width, space_height)
    win.setBackground(color_rgb(90, 5, 30))

    while True:
            for j in range(agents_numbers):
                x = agent[j][0]
                y = agent[j][1]

                v_x = agent[j][2]
                v_y = agent[j][3]
                # updating new positions
                x += v_x
                y += v_y

                # Periodic boundary implementation
                if x < 0:
                    x = x + space_width
                    # y = y - space_height

                if y < 0:
                    y = y + space_height
                    # x = x - space_width

                if x > space_width:
                    x = x - space_width

                if y > space_height:
                    y = y - space_height

                agent[j][0] = x
                agent[j][1] = y

                pt = Point(x, y)
                pt.setOutline(color_rgb(240, 230, 190))
                pt.draw(win)

                agent_history[j][0] = rotate(agent_history[j][0], 1)
                agent_history[j][1] = rotate(agent_history[j][1], 1)

                agent_history[j][0][0] = agent[j][0]
                agent_history[j][1][0] = agent[j][1]

                q = Point(agent_history[j][0][tail], agent_history[j][1][tail])
                q.setOutline(color_rgb(90, 5, 30))
                q.draw(win)

            # here is the main! code:
            for t in range(agents_numbers):
                # searching for other agents in the interaction area of agent[t]
                mean_x = 0
                mean_y = 0

                mean_pt_x = 0
                mean_pt_y = 0
                interacting_agents = 0
                for q in range(agents_numbers):
                        if distance(agent[t], agent[q]) <= interaction_radius:
                            interacting_agents += 1
                            mean_x += agent[q][2]
                            mean_y += agent[q][3]

                agent[t][4] = mean_x / interacting_agents
                agent[t][5] = mean_y / interacting_agents

                '''for p in range(agents_numbers):
                    potential = lennard(agent[t], agent[p])
                    if potential == 0:
                        agent[t][6] = 0
                        agent[t][7] = 0
                    else:
                        mean_pt_x += -1 * potential * (agent[p][0] - agent[t][0]) / (distance(agent[p], agent[t]))
                        mean_pt_y += -1 * potential * (agent[p][1] - agent[t][1]) / (distance(agent[p], agent[t]))'''
                # if mean_pt_x > max_vel:
                #    agent[t][6] = max_vel
                # else:
                #    agent[t][6] = mean_pt_x
                # if mean_pt_y > max_vel:
                #    agent[t][7] = mean_pt_y
                # else:
                #    agent[t][7] = mean_pt_y

            # agent[t][4] is the new value for agent[t][3]
            # but can't update it in the above loop because it will affect other agent's angle calculation
            for i in range(agents_numbers):

                agent[i][2] = w_mean * agent[i][4] + w_lenn * agent[i][6] + (random() - 0.5) * disturbance
                agent[i][3] = w_mean * agent[i][5] + w_lenn * agent[i][7] + (random() - 0.5) * disturbance

                # if agent[i][2] > max_vel:
                #     agent[i][2] = max_vel

                # if agent[i][3] > max_vel:
                #     agent[i][3] = max_vel


main()
